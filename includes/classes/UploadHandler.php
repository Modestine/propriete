
<?php
class UploadHandler
{

  private $con;

  public function __construct($con)
  {
    $this->con = $con;
  }

  public function createUploadForm()
  {

    $title = $this->createTitle();

    $decription = $this->createDescription();
    $file = $this->createFile();
    $uploadButton = $this->createUploadButton();
    $type = $this->createType();
    $price = $this->createPrice();
    $ville = $this->createVille();


    return "<form action='processing.php' method='POST' enctype='multipart/form-data'>
    <div class='row'>
    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>

        <p>Les champs avec astérique sont obligatoires. </p>
                $title
                $type
                $price
                $ville
                $decription
                $file
                $uploadButton
                </div>
                </div>
              </div>
            </div>
              </div>  
        </form>";
  }

  private function createTitle()
  {
    return "

        <div class='input-field ' >
          <input id='title' type='text' data-length='25' name='title' required>
          <label for='title'>Titre *</label>
        </div>
    ";
  }

  private function createPrice()
  {
    return "<div class='input-field ' >
          <input id='price' type='text' data-length='25' name='price'>
          <label for='price'>Prix</label>
        </div>";
  }

  private function createDescription()
  {

    return " <div class='input-field'>
                  <textarea id='description' name='description' class='materialize-textarea' data-length='120' required></textarea>
                  <label for='description'>Description *</label>
                </div>
            ";
  }

  private function createFile()
  {
    return "
      
          <div class='file-field input-field'>
            <div class='btn'>
              <span>Photo 1*</span>
              <input type='file' name='file1' required>
            </div>
            <div class='file-path-wrapper'>
              <input class='file-path validate' type='text' id='file1' placeholder='Obligatoire'>
            </div>
          </div>

          <div class='file-field input-field'>
          <div class='btn'>
            <span>Photo 2</span>
            <input type='file' name='file2'>
          </div>
          <div class='file-path-wrapper'>
            <input class='file-path validate' type='text' id='file2' placeholder='Optionnel'>
          </div>
        </div>

        <div class='file-field input-field'>
        <div class='btn'>
          <span>Photo 3</span>
          <input type='file' name='file3' >
        </div>
        <div class='file-path-wrapper'>
          <input class='file-path validate' type='text' id='file3' placeholder='Optionnel'>
        </div>
      </div>";
  }

  private function createUploadButton()
  {
    return "  <div class='center-align'> <input type='submit' name='uploadButton' id='uploadButton' class='btn teal waves-effect waves-light ' value='Publier'></div>";
  }

  private function createType()
  {
    return " 
    <label  for='type'>Type *</label>
    <select name='type'>
      <option value='1'>Maison à vendre</option>
      <option value='2'>Maison à louer</option>
      <option value='3'>Appartement</option>
      <option value='4'>Hotel</option>
      <option value='5'>Appareil éléctronique</option>
      <option value='6'>Téléphone</option>
      <option value='7'>Autres</option>
    </select>";
  }


  private function createVille()
  {
    return " 
    <label  for='type'>Ville (choisissez votre ville) *</label>
    <select name='ville'>
      <option value='1'>Lubumbashi</option>
      <option value='2'>Kinshasa</option>
      <option value='3'>Mbuji-Mayi</option>
      <option value='4'>Kananga</option>
      <option value='5'>Kisangani</option>
      <option value='6'>Bukavu</option>
      <option value='7'>Tshikapa</option>
      <option value='8'>Kolwezi</option>
      <option value='9'>Likasi</option>
      <option value='10'>Goma</option>
      <option value='11'>Kipushi</option>
    </select>";
  }

  private function createNumberOfRooms()
  {
    return "
    <label for='numberOfRooms'>Nombre de chambres</label>
    <select name='numberOfRooms'>
      <option value='1'>1</option>
      <option value='2'>2</option>
      <option value='3'>3</option>
      <option value='4'>4</option>
      <option value='5'>5</option>
      <option value='6'>6</option>
      <option value='7'>7</option>
      <option value='8'>8</option>
      <option value='9'>9</option>
      <option value='10'>10</option>
      <option value='11'>11</option>
      <option value='12'>12</option>
      <option value='13'>13</option>
      <option value='14'>14</option>
      <option value='15'>15</option>
    </select>";
  }
}

?>