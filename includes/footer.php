<script src="plugins/jQuery/jQuery.min.js"></script>
<script src="plugins/materialize/materialize.min.js"></script>
<script src="js/init.js"></script>

<script>
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('sw.js', {
        scope: "./"
      })
      .then(function() {
        console.log('Service worker registered');
      });
  }
</script>
<!--  testing offline page -->
<!-- <script>
if ("serviceWorker" in navigator) {
  if (navigator.serviceWorker.controller) {
    console.log("[PWA Builder] active service worker found, no need to register");
  } else {
    // Register the service worker
    navigator.serviceWorker
      .register("pwabuilder-sw.js", {
        scope: "./"
      })
      .then(function (reg) {
        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
      });
  }
}
</script> -->

<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>

<script>
  function developpement() {
    alert("Salut, Cette fonctionalite est en cours de developpement, veuillez-nous contacter pour en savair d'avantage");
  }

  function goBack() {
  window.history.back()
}
</script>

</body>

</html>