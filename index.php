<?php
require_once("includes/header.php");
?>

<style scoped>
  .pbo-1 {
    padding-bottom: 1em !important;
  }
</style>
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <h1 class="header center orange-text hide-on-med-and-down">Modestine</h1>
    <div class="row center">
      <!-- <h5 class="header col s12 light">Njia rahisi sana ya kuuza au kukodisha mali yako.</h5>
      <h5 class="header col s12 light">Lolenge ya pete pona koteka to kodefisa biloko na bino.</h5> -->
      <h5 class="header col s12 light">Un moyen trés simple et gratuit pour vendre ou faire louer vos biens.</h5>
    </div>

    <div class="row center">
      <a href="articles.php" id="download-button" class="btn-large waves-effect waves-light black-text lighten-4 red ">Voir les articles déjà publiés.</a>
    </div>
    <div class="row center">
      <a href="signin.php" id="download-button" class="btn-large waves-effect  black-text  orange lighten-4">Connectez-vous pour publier.</a>
    </div>
    <!-- <div class="row center" id="InstallApp">
      <span  class="btn-large waves-effect  black-text  orange lighten-4">Install App</span>
    </div> -->
    <br>

  </div>
</div>

<div class="fixed-action-btn hide-on-large-only" style="margin-bottom: 55px;">
  <a class=" " href="https://wa.me/27640490820/?text=Bienvenu, ecrivez-nous si vous avez une question.">
    <img src="img/whatsappicon48.png" alt="whatsapp">
  </a>
</div>

<div class="fixed-action-btn hide-on-med-and-down" style="margin-bottom: 55px;">
  <a class="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=contact@modestine.com" >
  <i class="medium material-icons green-text">email</i>
  </a>
</div>

<div class="grey lighten-3">
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center teal-text"><i class="material-icons">attach_money</i></h2>
            <h5 class="center">Comment Vendre?</h5>
            <p class="light">Inscrivez pour etre capable de publier les biens que vous souhaitez vendre, faire louer ou faire une donnation et être notifier lorsqu'une personne veut de votre bien.</p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center teal-text"><i class="material-icons">pan_tool</i></h2>
            <h5 class="center">Devener Sponsor!</h5>

            <p class="light">Sponsoriser notre projet de developpement et d'amelioration de notre produit.<br>Tout sponsor aparaitra sur notre site internet pour un surcroît de notoriété en guise de remerciement.</p>
          </div>
        </div>

        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center teal-text"><i class="material-icons">group</i></h2>
            <h5 class="center">Devener Partenaire!</h5>

            <p class="light">Vous avez une entreprise et vous voulez commencer à vendre vos bien sur notre site?
              <br>Vous êtes developpeur ou programmeur et vous voulez entrer en partenariat avec nous?<br><a href="https://wa.me/27640490820/?text=Bienvenu, ecrivez-nous si vous avez une question.">veuillez nous contactez!</a></p>
          </div>
        </div>
        <div class="col s12 m3">
          <div class="icon-block">
            <h2 class="center teal-text"><i class="material-icons">info_outline</i></h2>
            <h5 class="center">Comment Installer</h5>

            <p class="light">Vous pouvez ajouter ce site sur votre écran et l'utiliser comme une application native.<br>
              Apartir de votre navigateur , cliquez sur le 3 points verticales, choisissez </span><span class=""> Ajouter à l'écran d'accueil.</span><br><a href="#commentinstaller">Voir la vidéo.</a></p>
          </div>
        </div>
      </div>

    </div>
    <br>
  </div>
</div>
<div class="grey lighten-2">
  <div class="container">
    <div class="section">

      <div class="carousel pb-10 ">
        <a class="carousel-item" href="#one!"><img src="img/acceuil/1.PNG" height="250"></a>
        <a class="carousel-item" href="#two!"><img src="img/acceuil/2.PNG" height="250"></a>
        <a class="carousel-item" href="#three!"><img src="img/acceuil/3.PNG" height="250"></a>
        <a class="carousel-item" href="#four!"><img src="img/acceuil/4.PNG" height="250"></a>
        <a class="carousel-item" href="#five!"><img src="img/acceuil/5.PNG" height="250"></a>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12">
    <h5 class="center">Project A Venir.</h5>
    </div>
  </div>
  <div class="video-container"  id="commentinstaller">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/nraji9LzwJM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
  <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/4DhjGQ_zxrw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
  </div>
  <hr>
</div>

<div class="grey lighten-3 pt-10">
  <div class="container">
    <div class="section">

      <h5 class="center">NOS SPONSORS.</h5>
      <br /><br />
      <div class="row">
        <div class="col s12 m3 pbo-1">
          <div class="icon-block center">
          <a href="http://hazetu.com" >
            <img src="img/HAZETU.png" width="150" alt="Logo Sponsor" class="roundedImage"><br>
            <span class="blue-text" >Voir plus de details</span>

            </a>
          </div>
        </div>

        <div class="col s12 m3 pbo-1">
          <div class="icon-block center">
          <a href="http://modestinewebservices.com" >
            <img src="img/LOGOLOGO.jpg" width="150" alt="Logo Sponsor" class="roundedImage"><br>
            <span class="blue-text" >Voir plus de details</span>
            </a>
          </div>
        </div>

        <div class="col s12 m3 pbo-1">
          <div class="icon-block center">
          <a href="https://wa.me/27640490820/?text=Salut, J'aimerai sponsoriser votre application.">
            <img src="img/Sponsor-Logo.png" width="150" alt="Logo Sponsor" class="roundedImage"><br>
            <span class="blue-text" >Devener un sponsor</span>
            </a>
          </div>
        </div>

        <div class="col s12 m3 pbo-1">
          <div class="icon-block center">
          <a href="https://wa.me/27640490820/?text=Salut, J'aimerai sponsoriser votre application." >
            <img src="img/Sponsor-Logo.png" width="150" alt="Logo Sponsor" class="roundedImage"><br>
            <span class="blue-text" >Devener un sponsor</span>
            </a>
          </div>
        </div>
      </div>
      <br>
    </div>
  </div>

  <footer class="page-footer teal" style="margin">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <!-- <h5 class="white-text">A propos de nous</h5> -->
          <h5 class="white-text">Modestine Web Services</h5>
          <p class="grey-text text-lighten-4">Nous sommes une PME basée en RDC qui travaille dans le domaine de la technologie pour
            apporter des solutions aux problèmes en Afrique plus précisement en RDC.
            Abonnez-vous sur nos réseaux sociaux pour être toujours à jour sur nos offres d'emplois et projets à venir!
          </p>


        </div>
<!-- 
        <div class="col l3 s12">
          <h5 class="white-text">Connexion</h5>
          <ul>
            <li><a class="white-text" href="www.linkedin.com/in/modestine-web-43220a194/">Linkedin</a></li>
            <li><a class="white-text" href="web.facebook.com/Modestine-Information-Technology-Service-218074451582937/">Twitter</a></li>
            <li><a class="white-text" href="web.facebook.com/Modestine-Information-Technology-Service-218074451582937/">Instagram</a></li>
            <li><a class="white-text" href="web.facebook.com/Modestine-Information-Technology-Service-218074451582937/">Facebook</a></li>
          </ul>
        </div> -->

        <div class="col l3 s12 right">
          <h5 class="white-text">Contact</h5>
          <ul>
            <li><a class="white-text" href="#!">Phone:</a></li>
            <li><a class="white-text" href="tel:243993643887">+243993643887</a></li>
            <li><a class="white-text" href="tel:243974114676">+243974114676</a></li>
            <li><a class="white-text" href="tel:27813516102">+27813516102</a></li>
            <li><a class="white-text" href="#!">Email:</a></li>
            <li><a class="white-text" href="mailto:contact@modestine.com">contact@modestine.com</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright" style="    margin-bottom: 55px;">
      <div class="container">
        <span class="text-black">&copy; 2018 Modestine Web Services &nbsp;&nbsp; <a class="white-text" href="http://modestinewebservices.com">www.modestinewebservices.com</a></span>
      </div>
    </div>
  </footer>

  <?php require_once("includes/appfooter.php"); ?>
  <?php require_once("includes/footer.php"); ?>

