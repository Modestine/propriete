<?php
require_once("includes/header.php");
require_once("includes/classes/HomeHandler.php");

// if (isset($_SESSION["userLoggedIn"]) || isset($_COOKIE["UjishaCookie"])) {
  if (!isset($_COOKIE["UjishaC"])) {
  header("Location: signin.php");
}

$HomeHandler = new HomeHandler($con);
$userId = $userLoggedInObj->getUserId();
$properties = $HomeHandler->getProperties($userId);
$property1 = new Property($con, $properties, null);

?>
<div class="container mb-70">

  <?php

  if (sizeof($properties) > 0) {

    echo $property1->generateItemsFromMyproperties($properties);
  } else {
    echo " <div class='row'>
        <div class='col s12 '>
          <div class='card grey lighten-3'>
            <div class='card-content black-text'>
              <span class='card-title'>Oh la la!</span>
              <p>Vous n'avez pas encore publier un article sur votre compte.</p>
            </div>
            <div class='card-action'>
            <a href='publier.php' id='download-button' class='btn-large waves-effect waves-light black-text lighten-4 red '>Publier.</a>
              
            </div>
          </div>
        </div>
      
      
      <div class='col s12 '>
      <div class='card blue-grey darken-1'>
        <div class='card-content white-text'>
          <span class='card-title'>Publicité Sponsor ici.</span>
          <p>Sponsoriser notre projet de developpement et d'amelioration de notre produit.<br>Tout sponsor aparaitra sur notre site internet
           pour un surcroît de notoriété en guise de remerciement.</p>
        </div>
        <div class='card-action'>
          <a href='#'>Voir plus de details</a>
        </div>
      </div> 
    </div>
    </div>";
  }

  ?>

  <div class="fixed-action-btn hide-on-small-only">
    <a class="btn-floating btn-large teal" href="publier.php">
      <i class="large material-icons">add</i>
    </a>
    <!-- <ul>
    <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>

  </ul> -->
  </div>


</div>
<?php require_once("includes/appfooter.php"); ?>


<?php require_once("includes/footer.php"); ?>