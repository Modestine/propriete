<?php

class SearchHandler {
    private $con, $userLoggedInObj;

    public function __construct($con, $userLoggedInObj) {
        $this->con = $con;
        $this->userLoggedInObj = $userLoggedInObj;

    }

    public function getProperties($text, $orderBy) {
        $query = $this->con->prepare("      SELECT 
        `properties`.`id`,
        `properties`.`userId`,
        `properties`.`type`,
        `properties`.`price`,
        prt.name,
        `properties`.`uploadedBy`,
        `properties`.`title`,
        `properties`.`description`,
        `properties`.`filePath1`,
        `properties`.`filePath2`,
        `properties`.`filePath3`,
        `properties`.`category`,
        `properties`.`uploadDate`,
        `properties`.`views`,
        `properties`.`property_type_id`
         FROM properties LEFT JOIN property_type prt ON prt.id = properties.property_type_id  WHERE title LIKE CONCAT('%', :text, '%')
        OR uploadedBy LIKE CONCAT('%', :text, '%') OR description LIKE CONCAT('%', :text, '%')  ORDER BY $orderBy DESC") ;

        $query->bindParam(":text", $text);
        $query->execute();

        $properties = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $property = new Property($this->con, $row, $this->userLoggedInObj);
            array_push($properties, $property);
        }

        return $properties;
    }
}
