
ALTER TABLE `modestine_propriete`.`properties` 
ADD COLUMN `ville_id` INT(11) NOT NULL DEFAULT 1 AFTER `property_type_id`;

CREATE TABLE `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ville` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ville` (`name_ville`) VALUES ('Lubumbashi');
INSERT INTO `ville` (`name_ville`) VALUES ('Kinshasa');
INSERT INTO `ville` (`name_ville`) VALUES ('Mbuji-Mayi');
INSERT INTO `ville` (`name_ville`) VALUES ('Kananga');
INSERT INTO `ville` (`name_ville`) VALUES ('Kisangani');
INSERT INTO `ville` (`name_ville`) VALUES ('Bukavu');
INSERT INTO `ville` (`name_ville`) VALUES ('Tshikapa');
INSERT INTO `ville` (`name_ville`) VALUES ('Kolwezi');
INSERT INTO `ville` (`name_ville`) VALUES ('Likasi');
INSERT INTO `ville` (`name_ville`) VALUES ('Goma');
INSERT INTO `ville` (`name_ville`) VALUES ('Kipushi');