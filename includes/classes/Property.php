<?php

class Property
{
    private $con, $sqlData, $userLoggedInObj;

    public function __construct($con, $input, $userLoggedInObj)
    {
        $this->con = $con;
        $this->userLoggedInObj = $userLoggedInObj;


        if (is_array($input)) {
            $this->sqlData = $input;
        } else {

            $query = $this->con->prepare("
        SELECT 
        `properties`.`id`,
        `properties`.`userId`,
        `properties`.`type`,
        `properties`.`price`,
        prt.name,
        user.phone_number,
        ville.name_ville,
        `properties`.`uploadedBy`,
        `properties`.`title`,
        `properties`.`description`,
        `properties`.`filePath1`,
        `properties`.`filePath2`,
        `properties`.`filePath3`,
        `properties`.`category`,
        `properties`.`uploadDate`,
        `properties`.`views`,
        `properties`.`property_type_id`
         FROM properties 
         LEFT JOIN property_type prt ON prt.id = properties.property_type_id 
         LEFT JOIN ville ON ville.id = properties.ville_id 
         LEFT JOIN users user ON user.id = properties.userId 
         WHERE properties.id = :id ");
            $query->bindParam(":id", $input);
            $query->execute();

            $this->sqlData = $query->fetch(PDO::FETCH_ASSOC);

        }
    }


    public function create($property)
    {

        $url = "view.php?id=" . $property->getId();
        $imgsrc1 = $property->getFilePath1();
        // $imgsrc2 = $property->getFilePath2();
        // $imgsrc3 = $property->getFilePath3();

        $title = $property->getTitle();
        $description = $property->getDescription();
        $uploadedBy = "Publier par " . $property->getUploadedBy();
        $uploadDate = "Date: " . $property->getUploadDate();
        $views = "Nombre de vues: " . $property->getViews();
        $type = "Type: " . $property->getType();
        $price = "Prix: " . $property->getPrice();
        // $ville = "Ville: " . $property->getVille();


        if (isset($imgsrc1)) {
            return "
            <ul class='collection'>
            <li class='collection-item avatar'>
              <img src='$imgsrc1' alt='image' class='circle'>
              <p class='title' style='text-overflow: ellipsis; word-wrap: break-word; overflow: hidden; max-width: 70%';>$title</p>
              <p><strong>$price</strong><br>$type  <br>$uploadedBy <br>$uploadDate <br>$views
        
              </p>
              <a href='$url' class='secondary-content'><span class='waves-effect waves-light btn'>Voir</span></a>
            </li>
          </ul> ";
        } else {
            return "
            <ul class='collection'>
            <li class='collection-item avatar'>
              <img src='img/defaultimage.png' alt='image' class='circle'>
              <span class='title' style='text-overflow: ellipsis; word-wrap: break-word; overflow: hidden; max-width: 70%';>$title</span>
              <p>$type  <br>$uploadedBy <br>$uploadDate <br>$views
        
              </p>
              <a href='$url' class='secondary-content'><span class='waves-effect waves-light btn'>Voir</span></a>
            </li>
          </ul> ";
        }
    }

    public function mycreate($property)
    {

        $url = "edit.php?id=" . $property->getId();
        $imgsrc1 = $property->getFilePath1();
        // $imgsrc2 = $property->getFilePath2();
        // $imgsrc3 = $property->getFilePath3();

        $title = $property->getTitle();
        $description = $property->getDescription();
        $uploadedBy = "Publier par " . $property->getUploadedBy();
        $uploadDate = "Date: " . $property->getUploadDate();
        $views = "Nombre de vues: " . $property->getViews();
        $type = "Type: " . $property->getType();
        $price = "Prix: " . $property->getPrice();
        // $ville = "Ville: " . $property->getVille();

        if (isset($imgsrc1)) {
            return "
    <ul class='collection'>
    <li class='collection-item avatar'>
      <img src='$imgsrc1' alt='image' class='circle'>
      <span class='title' style='text-overflow: ellipsis; word-wrap: break-word; overflow: hidden; max-width: 70%';>$title</span>
      <p>$type  <br>$uploadedBy <br>$uploadDate <br>$views

      </p>
      <a href='$url' class='secondary-content'><span class='waves-effect waves-light btn'>Voir</span></a>
    </li>
  </ul> ";
        } else {
            return "
    <ul class='collection'>
    <li class='collection-item avatar'>
      <img src='img/defaultimage.png' alt='image' class='circle'>
      <span class='title' style='text-overflow: ellipsis; word-wrap: break-word; overflow: hidden; max-width: 70%';>$title</span>
      <p>$type  <br>$uploadedBy <br>$uploadDate <br>$views

      </p>
      <a href='$url' class='secondary-content'><span class='waves-effect waves-light btn'>Voir</span></a>
    </li>
  </ul> ";
        }
    }


    public function generateItemsFromproperties($properties)
    {

        $elementsHtml = "";

        foreach ($properties as $property) {
            $elementsHtml .= $this->create($property);
        }

        return $elementsHtml;
    }

    public function generateItemsFromMyproperties($properties)
    {

        $elementsHtml = "";

        foreach ($properties as $property) {
            $elementsHtml .= $this->mycreate($property);
        }

        return $elementsHtml;
    }

    public function getId()
    {
        return $this->sqlData["id"];
    }
    public function getUserNumber()
    {
        return $this->sqlData["phone_number"];
    }
    public function getUploadedBy()
    {
        return $this->sqlData["uploadedBy"];
    }

    public function getTitle()
    {
        return $this->sqlData["title"];
    }
    public function getDescription()
    {
        return $this->sqlData["description"];
    }
    public function getFilePath1()
    {
        return $this->sqlData["filePath1"];
    }

    public function getFilePath2()
    {
        return $this->sqlData["filePath2"];
    }

    public function getFilePath3()
    {
        return $this->sqlData["filePath3"];
    }
    public function getUploadDate()
    {
        return $this->sqlData["uploadDate"];
    }
    public function getPrice()
    {
        $ret = "Contacter pour le prix";
        if ($this->sqlData["price"] != null) {
            return $this->sqlData["price"];
        } else {
            return $ret;
        }
    }

    public function getVille()
    {
        return $this->sqlData["name_ville"];
    }
    public function getType()
    {
        return $this->sqlData["name"];
    }
    public function getViews()
    {
        return $this->sqlData["views"];
    }

    public function getUserId()
    {
        return $this->sqlData["userId"];
    }



    public function incrementViews()
    {
        $query = $this->con->prepare("UPDATE properties SET views=views+1 WHERE id=:id");
        $query->bindParam(":id", $propertyId);

        $propertyId = $this->getId();
        $query->execute();

        $this->sqlData["views"] = $this->sqlData["views"] + 1;
    }


    public function deleteArticle()
    {
        $userId = $this->getUserId();
        if (isset($userId)) {
            $propertyId = $this->getId();
            $query = $this->con->prepare("SELECT id FROM properties WHERE id=$propertyId AND userId = $userId;");
            $propertyIdToDelete = $query->execute();
            if (isset($propertyIdToDelete)) {
                $query1 = $this->con->prepare("DELETE FROM properties WHERE id=$propertyId;");
                $articleDelteted = $query1->execute();
                return true;
            } else {
                echo "  <div class='row'>
                <div class='col s12'>
                  <div class='card white darken-1'>
                    <div class='card-content green-text'>
                      <h5 class='center-align'><span class='card-title '><i class='material-icons Large'>check_circle</i></span></h5><br>
                      <h5 class='center-align'>Vous n'êtes pas autorisés à éffectué cette action.</h5>
                    </div>
                    <div class='card-action'>
                      <a href='articles.php' class='blue-text'>Cliquez Ici pour retourner aux articles.</a>
                    </div>
                  </div>
                </div>
                </div>
                
                <div class='progress'>
                <div class='indeterminate'></div>
                </div>";
                return false;
            }
            return true;
        } else {
            echo "  <div class='row'>
            <div class='col s12'>
              <div class='card white darken-1'>
                <div class='card-content green-text'>
                  <h5 class='center-align'><span class='card-title '><i class='material-icons Large'>check_circle</i></span></h5><br>
                  <h5 class='center-align'>Vous n'êtes pas autorisés à éffectué cette action.</h5>
                </div>
                <div class='card-action'>
                  <a href='articles.php' class='blue-text'>Cliquez Ici pour retourner aux articles.</a>
                </div>
              </div>
            </div>
            </div>
            
            <div class='progress'>
            <div class='indeterminate'></div>
            </div>";
            return false;
        }
    }
}
