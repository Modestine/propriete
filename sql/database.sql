CREATE DATABASE  IF NOT EXISTS `ujisha_propriete`;
USE `ujisha_propriete`;



DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `uploadedBy` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `filePath1` varchar(250) DEFAULT NULL,
  `filePath2` varchar(250) DEFAULT NULL,
  `filePath3` varchar(250) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `uploadDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `views` int(11) DEFAULT '0',
  `property_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `property_type`;

CREATE TABLE `property_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


LOCK TABLES `property_type` WRITE;
/*!40000 ALTER TABLE `property_type` DISABLE KEYS */;
INSERT INTO `property_type` VALUES (1,1,'Maison a vendre'),(2,2,'Maison a louer'),(3,3,'Appartement '),(4,4,'Hotel '),(5,5,'Appareil electronique'),(6,6,'Telephone'),(7,7,'Autres ');

UNLOCK TABLES;

--
-- Table structure for table `reseau_type`
--

DROP TABLE IF EXISTS `reseau_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reseau_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reseau_type`
--

LOCK TABLES `reseau_type` WRITE;
/*!40000 ALTER TABLE `reseau_type` DISABLE KEYS */;
INSERT INTO `reseau_type` VALUES (1,'Vodacom'),(2,'Airtel'),(3,'Orange'),(4,'Africel'),(5,'Tigo'),(6,'Autre');
/*!40000 ALTER TABLE `reseau_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `signUpDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `profilePic` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `reseau_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

