<?php

ob_start(); //Turns on output buffering 
session_start();

date_default_timezone_set("Africa/Harare");
define('SITE_ROOT', __DIR__);

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080") {
    try {

        $con = new PDO("mysql:dbname=ujisha_propriete;host=localhost", "root", "LOv1p3r");
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
} else {
    try {

        // $con = new PDO("mysql:dbname=dbs168238;host=db5000173359.hosting-data.io", "dbu255707", "Shirua1502@");
        $con = new PDO("mysql:dbname=ujisha_propriete;host=localhost", "ujisha", "Ujisha@1502");

        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage() . "no connection oops";
    }
}


// CREATE USER 'ujisha'@'localhost' IDENTIFIED BY 'Ujisha@1502';

// GRANT ALL PRIVILEGES ON * . * TO 'ujisha'@'localhost';

// FLUSH PRIVILEGES;
