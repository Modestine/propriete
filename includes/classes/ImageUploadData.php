<?php
class ImageUploadData {

    public $title, $type, $price, $description, $ville_id, $file1, $file2, $file3, $userId, $uploadedBy;

    public function __construct($title, $type, $price, $description, $ville_id, $file1, $file2, $file3, $userId, $uploadedBy){

        $this->title = $title;
        $this->type = $type;
        $this->price = $price;
        $this->description = $description;
        $this->ville_id = $ville_id;
        $this->file1 = $file1;
        $this->file2 = $file2;
        $this->file3 = $file3;
        $this->userId = $userId;
        $this->uploadedBy = $uploadedBy;
        

    }

}

?>