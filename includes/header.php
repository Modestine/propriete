<?php
require_once("includes/config.php");
require_once("includes/classes/User.php");
require_once("includes/classes/Property.php");
require_once("includes/classes/ImageGrid.php");
require_once("includes/classes/ImageGridItem.php");
require_once("includes/classes/Account.php");

if (isset($_COOKIE["UjishaC"])) {
  $usernameLoggedIn = $_COOKIE["UjishaC"];
  $userLoggedInObj = new User($con, $usernameLoggedIn);
} else {
  // $userLoggedInObj = new User($con, null);
}
?>

<html lang="fr">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148384626-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-148384626-1');
  </script>
  <meta http-equiv="Content-Security-Policy" content="script-src 'self' 'unsafe-inline' 'unsafe-eval' https://www.googletagmanager.com/ https://www.gstatic.com/ https://apis.google.com/ https://www.google-analytics.com/; object-src 'self';  default-src 'self'; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; media-src *; img-src 'self' https://www.google-analytics.com/ data: content:; font-src 'self' data: https://fonts.gstatic.com; connect-src 'self' https://fcm.googleapis.com  https://www.googleapis.com/ https://apis.google.com/ https://www.google-analytics.com/; frame-src https://www.youtube.com"> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>Modestine</title>

  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <link rel="stylesheet" type="text/css" href="plugins/materialize/materialize.min.css" media="screen,projection" />
  <link rel="manifest" href="manifest.json">
  <link href="css/font-icons.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="Modestine">
  <!-- <meta name="theme-color" content="none"> -->
 <meta name="apple-mobile-web-app-status-bar-style" content="black">
 <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <!-- <link rel="apple-touch-icon" href="img/pwa/icon-72.png" sizes="144x108"> -->
  <link rel="icon" href="img/favicon.png">

</head>

<body>
  <div class="navbar-fixed" style="z-index:1000;">
    <nav class="teal lighten-1" role="navigation">
      <div class="nav-wrapper pl-20 pr-20"><a id="logo-container" href="#" class="brand-logo">Modestine</a>
        <ul class="right hide-on-med-and-down">
          <li><a href="index.php">Acceuil</a></li>
          <li><a href="articles.php">Articles</a></li>
          <li><a href="home.php">Publications</a></li>
          <li><a href="search.php">Recherche</a></li>
          <li><a href="publier.php">Publier</a></li>
          <li class="enable-notifications"><a> <i class="material-icons">notifications</i></a></li>
          <li class="red-text">
          <?php if (isset($_COOKIE["UjishaC"])) 
          {
            echo $userLoggedInObj->getFirstName(); 
          } 
          ?>
          </li>
          <?php
          if (!isset($_COOKIE["UjishaC"])) 
          {
            echo "<li><a class='waves-effect waves-light btn-small teal' href='signin.php' >Connexion</a></li>";
          } else {
            echo "<li><a class='waves-effect waves-light btn-small teal' href='signout.php' >Deconnexion</a></li>";
          }
          ?>
        </ul>
        <ul id="nav-mobile" class="sidenav">
          <li>
            <div class="user-view">
              <div class="background teal lighten-2">
              </div>
              <a href="#user"><img class="circle" src="img/pwa/icon-1024.png"></a>
              <p>
                <?php
                if (!isset($_COOKIE["UjishaC"])) {
                  echo " <span>Modestine</span>";
                } else {
                  echo  $userLoggedInObj->getFullName();;
                }
                ?>
            </div>
          </li>
          <li><a href="index.php"> <i class="material-icons">home</i> Acceuil</a></li>
          <li><a href="home.php"> <i class="material-icons">accessibility</i> Mes Publications</a></li>
          <li><a href="articles.php"> <i class="material-icons">shopping_cart</i> Articles</a></li>
          <li><a href="search.php"> <i class="material-icons">search</i> Rechercher</a></li>
          <li><a href="#"> <i class="material-icons">shopping_cart</i> A Vendre</a></li>
          <!-- <li><a href="#"> <i class="material-icons">hotel</i> A Louer</a></li> -->
          <li class="enable-notifications"><a> <i class="material-icons">notifications</i> notifications</a></li>
          <?php
          if (!isset($_COOKIE["UjishaC"])) {
            echo "<li><a href='signin.php'> <i class='material-icons'>highlight_off</i> Connextion</a></li>";
          } else {
            echo "<li><a href='signout.php'> <i class='material-icons'>highlight_off</i> Deconnecter</a></li>";
          }
          ?>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      </div>
    </nav>
  </div>