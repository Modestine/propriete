<?php

class ImageGrid {
    private $con, $userLoggedInObj;

    public function __construct($con, $userLoggedInObj) {
        $this->con = $con;
        $this->userLoggedInObj = $userLoggedInObj;

    }

    public function create($property){


        return "
        <ul class='collection'>
        <li class='collection-item avatar'>
          <img src='uploads/images/5d2b53eb84e98icons8-whatsapp-96.png' alt='' class='circle'>
          <span class='title'>Title</span>
          <p>First Line <br>
             Second Line
          </p>
          <a href='#!' class='secondary-content'><i class='material-icons'>grade</i></a>
        </li>
      </ul> ";
    }

    public function generateItemsFromproperties($properties){

        $elementsHtml = "";

        foreach ($properties as $property) {
        $elementsHtml .= $this->create($property);
        }

        return $elementsHtml;

    }
}
